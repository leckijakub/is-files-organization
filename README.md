# is-files-organization

Indexed-sequential files organization demo.

## Prerequisites

* cmake - Tested on `3.16.3`

* make - Tested on `4.2.1`

* readline package
  ```
  sudo apt-get install libreadline-dev
  ```

## Build

```shell
$ mkdir build && cd build
```
```shell
/build$ cmake ..
```

```shell
/build$ make
```

## Usage

 ```shell
 /build$ ./src/is
```
