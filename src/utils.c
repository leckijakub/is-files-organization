#include "utils.h"
#include <stdbool.h>
#include <stdarg.h>
#include <stdio.h>

bool Verbose = false;

void set_verbose(bool verbose){
    Verbose = verbose;
}

int verbose(const char *fmt, ...){
    if(!Verbose){
        return 0;
    }

    va_list args;
    va_start(args, fmt);

    printf("[");
    printf(COLOR_BOLD_GREEN "VERBOSE");
    printf(COLOR_NORMAL "]: ");
    int ret = vprintf(fmt, args);
    printf("\n");
    va_end(args);

    return ret;
}

bool is_verbose(){
    return Verbose;
}
