#include <stdio.h>
#include "is.h"
#include <getopt.h>
#include <stdlib.h>

#include "shell.h"

int main(int argc, char **argv)
{
    // Parsing commands Interactive mode or Script Mode
    if (argc == 4) {
        PAGE_SIZE = atoi(argv[1]);
        REORG_TRIG = atof(argv[2]);
        ALPHA = atof(argv[3]);
        is = is_new("index", "primary", "overflow");
        shell_interact();
    } else if (argc == 5) {
        PAGE_SIZE = atoi(argv[2]);
        REORG_TRIG = atof(argv[3]);
        ALPHA = atof(argv[4]);
        is = is_new("index", "primary", "overflow");
        shell_script(argv[1]);
    } else
        printf("\nInvalid Number of Arguments\n");

    // Exit the Shell
    return EXIT_SUCCESS;
}
