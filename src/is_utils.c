#include "is_utils.h"

struct RecPtr RecPtr_empty()
{
    struct RecPtr a = { .page_no = 0, .offset = 0, .empty = true };
    return a;
}
