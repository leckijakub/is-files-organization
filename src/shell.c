#include <stdlib.h>
#include <string.h>
#include <readline/readline.h>
#include <readline/history.h>
#include "shell.h"
#include "is.h"

// Definitions
char SHELL_NAME[50] = "is-shell> ";
int QUIT = 0;

int numBuiltin();

char *shell_cmd[] = { "insert",     "update", "delete",  "read", "show",
                      "reorganise", "exit",   "history", "help" };
int (*shell_func[])(char **) = {
    &shell_insert,     &shell_update, &shell_delete,  &shell_read, &shell_show,
    &shell_reorganise, &shell_exit,   &shell_history, &shell_help
}; // Array of function pointers for call from execShell

int shell_history(char **args)
{
    if (args[1] != NULL && strcmp(args[1], "help") == 0) {
        printf("%s [help]\n", args[0]);
        return 0;
    }
    /* get the state of your history list (offset, length, size) */
    HISTORY_STATE *myhist = history_get_history_state();

    /* retrieve the history list */
    HIST_ENTRY **mylist = history_list();

    if (myhist == NULL || mylist == NULL) {
        return 1;
    }

    printf("\nsession history\n\n");
    for (int i = 0; i < myhist->length; i++) { /* output history list */
        printf(" %8s  %s\n", mylist[i]->line, mylist[i]->timestamp);
    }
    putchar('\n');

    // free(myhist); /* free HIST_ENTRY list */
    // free(mylist); /* free HISTORY_STATE   */
    return 0;
}

int shell_reorganise(char **args)
{
    int argc = 0;

    set_verbose(false);

    for (int i = 1; args[i] != NULL; i++) {
        char *arg = args[i];
        argc++;

        if (strcmp(arg, "help") == 0) {
            printf("%s [help | verbose] alpha \n", args[0]);
            return 0;
        } else if (strcmp(arg, "verbose") == 0) {
            set_verbose(true);
        }
    }

    if (argc < 1 + is_verbose()) {
        printf("Insufficient number of args\n");
        return 1;
    }
    args = &args[is_verbose()];
    is_reorganise(is, atof(args[1]));
    is_debug_print(is);
    return 0;
}

int shell_insert(char **args)
{
    if (args[1] == NULL)
        printf("%s command require at least one argument\n", args[0]);
    int argc = 0;

    set_verbose(false);

    for (int i = 1; args[i] != NULL; i++) {
        char *arg = args[i];
        argc++;

        if (strcmp(arg, "help") == 0) {
            printf("%s [help | verbose] c1.x c1.y c2.x c2.y c3.x c3.y key\n",
                   args[0]);
            return 0;
        } else if (strcmp(arg, "verbose") == 0) {
            set_verbose(true);
        }
    }
    if (argc < 7 + is_verbose()) {
        printf("Insufficient number of args\n");
        return 1;
    }
    args = &args[is_verbose()];
    struct Record rec = { .a = { atoi(args[1]), atoi(args[2]) },
                          .b = { atoi(args[3]), atoi(args[4]) },
                          .c = { atoi(args[5]), atoi(args[6]) } };
    is_insert_record(is, rec, atoi(args[7]));
    is_debug_print(is);
    return 0;
}

int shell_update(char **args)
{
    if (args[1] == NULL)
        printf("%s command require at least one argument\n", args[0]);
    int argc = 0;

    set_verbose(false);

    for (int i = 1; args[i] != NULL; i++) {
        char *arg = args[i];
        argc++;

        if (strcmp(arg, "help") == 0) {
            printf("%s [help | verbose] c1.x c1.y c2.x c2.y c3.x c3.y key\n",
                   args[0]);
            return 0;
        } else if (strcmp(arg, "verbose") == 0) {
            set_verbose(true);
        }
    }
    if (argc < 7 + is_verbose()) {
        printf("Insufficient number of args\n");
        return 1;
    }
    args = &args[is_verbose()];
    struct Record rec = { .a = { atoi(args[1]), atoi(args[2]) },
                          .b = { atoi(args[3]), atoi(args[4]) },
                          .c = { atoi(args[5]), atoi(args[6]) } };
    is_update_record(is, rec, atoi(args[7]), false);
    is_debug_print(is);
    return 0;
}

int shell_delete(char **args)
{
    int argc = 0;
    if (args[0] == NULL) {
        printf("%s command require at least one parameter\n", args[0]);
        return 1;
    }

    set_verbose(false);

    for (int i = 1; args[i] != NULL; i++) {
        char *arg = args[i];
        argc++;

        if (strcmp(arg, "help") == 0) {
            printf("%s [help | verbose] key_to_delete \n", args[0]);
            return 0;
        } else if (strcmp(arg, "verbose") == 0) {
            set_verbose(true);
        }
    }
    if (argc < 1 + is_verbose()) {
        printf("Insufficient number of args\n");
        return 1;
    }
    args = &args[is_verbose()];

    is_delete_record(is, atoi(args[1]));
    is_debug_print(is);
    return 0;
}
int shell_read(char **args)
{
    int argc = 0;
    if (args[0] == NULL) {
        printf("%s command require at least one parameter\n", args[0]);
        return 1;
    }

    for (int i = 1; args[i] != NULL; i++) {
        char *arg = args[i];
        argc++;

        if (strcmp(arg, "help") == 0) {
            printf("%s [help] key_to_read \n", args[0]);
            return 0;
        }
    }
    if (argc < 1) {
        printf("Insufficient number of args\n");
        return 1;
    }
    is_read_record(is, atoi(args[1]));
    return 0;
}

int shell_show(char **args)
{
    if (args[1] != NULL && strcmp(args[1], "help") == 0) {
        printf("%s [help]\n", args[0]);
        return 0;
    }
    is_print_sorted_records(is);
    return 0;
}
int shell_help()
{
    char *args[3];
    args[0] = (char *)malloc(sizeof(char) * 32);
    args[1] = (char *)malloc(sizeof(char) * 32);
    args[2] = NULL;
    printf("Availables commands:\n\n");

    for (int i = 0; i < numBuiltin();
         i++) // numBuiltin() returns the number of builtin functions
    {
        strcpy(args[0], shell_cmd[i]);
        strcpy(args[1], "help");
        if (strcmp(shell_cmd[i], "help") == 0) {
            continue;
        }
        (*shell_func[i])(args);
        // if (strcmp(args[0], shell_cmd[i]) ==
        //     0) // Check if user function matches builtin function name
        //     return (*shell_func[i])(
        //         args); // Call respective builtin function with arguments
    }
    return 0;
}
int shell_exit(char **args)
{
    if (args[1] != NULL && strcmp(args[1], "help") == 0) {
        printf("%s [help]\n", args[0]);
        return 0;
    }
    QUIT = 1;
    return 0;
}

int numBuiltin() // Function to return number of builtin commands
{
    return sizeof(shell_cmd) / sizeof(char *);
}

// Function to split a line into constituent commands
char **splitLine(char *line)
{
    char **tokens = (char **)malloc(sizeof(char *) * 64);
    char *token;
    char delim[10] = " \t\n\r\a";
    int pos = 0, bufsize = 64;
    if (!tokens) {
        printf("Buffer Allocation Error.\n");
        exit(EXIT_FAILURE);
    }
    token = strtok(line, delim);
    while (token != NULL) {
        tokens[pos] = token;
        pos++;
        if (pos >= bufsize) {
            bufsize += 64;
            tokens = (char **)realloc(tokens, bufsize * sizeof(char *));
            if (!tokens) // Buffer Allocation Failed
            {
                printf("Buffer Allocation Error.\n");
                exit(EXIT_FAILURE);
            }
        }
        token = strtok(NULL, delim);
    }
    tokens[pos] = NULL;
    return tokens;
}

int execShell(char **args)
{
    if (args[0] == NULL) {
        // Empty command
        return 1;
    }
    // Loop to check for builtin functions
    for (int i = 0; i < numBuiltin();
         i++) // numBuiltin() returns the number of builtin functions
    {
        if (strcmp(args[0], shell_cmd[i]) ==
            0) // Check if user function matches builtin function name
        {
            page_set_write(0);
            page_set_read(0);
            int ret = (*shell_func[i])(
                args); // Call respective builtin function with arguments
            printf("Total read/write = %d\n",
                   page_read_count() + page_write_count());
            return ret;
        }
    }
    printf("Couldn't find command %s\n", args[0]);
    return 1;
}

int valid_line(char *line, int size)
{
    for (int i = 0; i < size; i++) {
        if (line[i] != ' ' && line[i] != '\t' && line[i] != 0) {
            return 1;
        }
    }
    return 0;
}

int shell_interact()
{
    char *line;
    char **args;
    while (QUIT == 0) {
        // printf("%s> ", SHELL_NAME);
        line = readline(SHELL_NAME);
        if (!valid_line(line, strlen(line) + 1))
            continue;
        add_history(line);
        args = splitLine(line);
        execShell(args);
        free(line);
        free(args);
    }
    return 1;
}

int shell_script(char *filename)
{
    printf("Received Script. Opening %s", filename);
    FILE *fptr;
    char line[200];
    char **args;
    fptr = fopen(filename, "r");
    if (fptr == NULL) {
        printf("\nUnable to open file.");
        return 1;
    } else {
        printf("\nFile Opened. Parsing. Parsed commands displayed first.");
        while (fgets(line, sizeof(line), fptr) != NULL) {
            printf("\n%s", line);
            args = splitLine(line);
            execShell(args);
        }
    }
    free(args);
    fclose(fptr);
    return 1;
}
