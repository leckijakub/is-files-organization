#include "page.h"

#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int Read_counter;
int Write_counter;


int page_read_count()
{
    return Read_counter;
}
int page_write_count()
{
    return Write_counter;
}

void page_set_read(int read)
{
    Read_counter = read;
}

void page_set_write(int write)
{
    Write_counter = write;
}

int page_count(FILE *file, int page_size)
{
    int cur_pos = ftell(file);
    fseek(file, 0, SEEK_END);
    int file_end = ftell(file);
    fseek(file, cur_pos, SEEK_SET);
    return file_end / page_size;
}

bool page_can_read_page(FILE *file, int page_size, int page_no)
{
    int pages_num = page_count(file, page_size);
    if (page_no >= pages_num) {
        PRINT_DEBUG("Page number exceed file size");
        return false;
    }
    return true;
}

int page_write_to_file(FILE *file, int page_size, int page_no, void *source,
                       int count, int record_size)
{
    struct PageHeader header;
    struct StoredRecord *begin = source;
    char *buf;
    int offset;
    int to_write = count;
    int max_rec_on_page = (page_size - PAGE_HEADER_SIZE) / record_size;

    int file_pos = page_size * page_no;
    fseek(file, file_pos, SEEK_SET);
    /* write page */
    do {
        /* set header */
        if (to_write > max_rec_on_page)
            header.records_on_page = max_rec_on_page;
        else
            header.records_on_page = to_write;

        /* write header */
        fwrite(&header, sizeof(struct PageHeader), 1, file);
        if (ferror(file) != 0) {
            PRINT_ERR("Error writing page header: %s", strerror(errno));
            return 0;
        }
        /* write records */
        fwrite(begin, record_size, header.records_on_page, file);
        if (ferror(file) != 0) {
            PRINT_ERR("Error writing page records: %s", strerror(errno));
            return 0;
        }
        /* write offset */
        offset = page_size - sizeof(struct PageHeader) -
                 header.records_on_page * record_size;

        buf = (char *)malloc(sizeof(char) * offset);

        memset(buf, 0, offset);
        fwrite(buf, 1, offset, file);
        if (ferror(file) != 0) {
            PRINT_ERR("Error writing page offset: %s", strerror(errno));
            free(buf);
            return 0;
        }
        free(buf);
        begin += header.records_on_page;
        to_write -= header.records_on_page;

        Write_counter++;
    } while (to_write > 0);

    return count;
}

int page_read_from_file(FILE *file, int page_size, int page_no, void *dest,
                        int record_size)
{
    struct PageHeader header;
    size_t result;
    int offset;

    // /* find if page_no isn't outside file */
    // if (!page_can_read_page(file, page_size, page_no)) {
    //     PRINT_DEBUG("Page number exceed file size");
    //     return 0;
    // }

    int new_pos = page_size * page_no;
    if (fseek(file, new_pos, SEEK_SET) != 0) {
        PRINT_DEBUG("Couldn't read page nr %d" page_no);
        return 0;
    }

    /* read page heager */
    result = fread(&header, sizeof(struct PageHeader), 1, file);

    if (feof(file)) {
        PRINT_DEBUG("EOF occured.");
        return 0;
    }
    if (ferror(file) != 0) {
        PRINT_ERR("Error reading page header: %s", strerror(errno));
        return 0;
    }

    /* read records from page */
    result = fread(dest, record_size, header.records_on_page, file);
    if (result != header.records_on_page) {
        PRINT_ERR("Error reading page records: %s", strerror(errno));
        return 0;
    }

    /* discard rest of page */
    offset = page_size - sizeof(struct PageHeader) - result * record_size;
    fseek(file, offset, SEEK_CUR);

    Read_counter++;

    return result;
}
