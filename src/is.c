#include "is.h"
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "utils.h"

void is_read_metadata(struct IS *is, const char *filename)
{
    FILE *md = fopen(filename, "rb");
    if (md == NULL) {
        memset(&is->meta_data, 0, sizeof(struct IS_metadata));
        return;
    }
    fread(&is->meta_data, sizeof(struct IS_metadata), 1, md);
    fclose(md);
    return;
}

void is_update_metadata(struct IS *is)
{
    is->meta_data.index_pages = is->index->page_count;
    is->meta_data.index_records = is->index->records_count;
    is->meta_data.primary_pages = is->primary->page_count;
    is->meta_data.primary_records = is->primary->records_count;
    is->meta_data.overflow_pages = is->overflow->page_count;
    is->meta_data.overflow_records = is->overflow->records_count;
}

void is_save_metadata(struct IS *is, const char *filename)
{
    is_update_metadata(is);
    FILE *md = fopen(filename, "wb");
    if (md == NULL) {
        PRINT_ERR("Couldn't open %s file for metadata", filename);
        return;
    }
    fwrite(&is->meta_data, sizeof(struct IS_metadata), 1, md);
    fclose(md);
    return;
}

void is_apply_metadata(struct IS *is)
{
    is->index->page_count = is->meta_data.index_pages;
    is->index->records_count = is->meta_data.index_records;
    is->primary->page_count = is->meta_data.primary_pages;
    is->primary->records_count = is->meta_data.primary_records;
    is->overflow->page_count = is->meta_data.overflow_pages;
    is->overflow->records_count = is->meta_data.overflow_records;
}

struct IS *is_new(const char *id_filename, const char *prim_filename,
                  const char *of_filename)
{
    struct IS *is = (struct IS *)malloc(sizeof(struct IS));
    is->index = idf_new(id_filename, PAGE_SIZE);
    is->primary = rf_new(prim_filename, PAGE_SIZE);
    is->overflow = rf_new(of_filename, PAGE_SIZE);

    is_read_metadata(is, "is.dat");
    is_apply_metadata(is);

    struct Record rec = record_minimal();

    if (is->primary->page_count == 0) {
        rf_create_next_page(is->primary);
        rf_append_record(is->primary, rec, 0, RecPtr_empty());
        idf_create_next_page(is->index);
        idf_append_index_global(is->index, 0, 0);
        is_delete_record(is, 0);
    }

    if (is->overflow->page_count == 0) {
        rf_create_next_page(is->overflow);
    }
    return is;
}

void is_free(struct IS *is)
{
    idf_free(is->index);
    rf_free(is->primary);
    rf_free(is->overflow);
    free(is);
}

int is_insert_record(struct IS *is, struct Record record, Key key)
{
    bool last_in_primary = true;
    bool next_in_primary = true;
    int page_no = idf_find_page_to_insert(is->index, key);
    struct RecPtr last_ptr = { .page_no = page_no, .offset = 0, .empty = true };
    struct RecPtr next_ptr = { .page_no = page_no,
                               .offset = 0,
                               .empty = false };
    struct StoredRecord last, *next;

    rf_goto_page(is->primary, page_no);

    next = rf_get_first_record_on_page(is->primary);

    while (true) {
        if (next == NULL) {
            if (next_in_primary) {
                /* albo wolne miejsce albo pełna strona */
                if (rf_append_record(is->primary, record, key, RecPtr_empty())
                        .empty) {
                    if (rf_on_last_page(is->primary)) {
                        rf_create_next_page(is->primary);
                        rf_append_record(is->primary, record, key,
                                         RecPtr_empty());
                        idf_append_index_global(is->index, key,
                                                is->primary->present_page_num);
                        break;
                    } else { /* not on last page */
                        if (last.of_ptr.empty) {
                            rf_goto_last_page(is->overflow);
                            if ((last.of_ptr = rf_append_record(
                                     is->overflow, record, key, RecPtr_empty()))
                                    .empty) {
                                rf_create_next_page(is->overflow);
                                last.of_ptr = rf_append_record(
                                    is->overflow, record, key, RecPtr_empty());
                            }
                            /* zmien ptr w last */
                            if (last_in_primary) {
                                rf_set_record(is->primary, last_ptr,
                                              last.record, last.key,
                                              last.deleted, last.of_ptr);
                            } else { /* last not in primary */
                                rf_set_record(is->overflow, last_ptr,
                                              last.record, last.key,
                                              last.deleted, last.of_ptr);
                            }
                            break;
                        } else { /* last ptr is not empty */
                            next = rf_get_record_global(is->overflow,
                                                        last.of_ptr.page_no,
                                                        last.of_ptr.offset);
                            next_in_primary = false;
                            next_ptr = last.of_ptr;
                        }
                    }
                } else { /* succesful append to primary */
                    break;
                }
            } else { /* next not in primary */
                rf_goto_last_page(is->overflow);
                if ((last.of_ptr = rf_append_record(is->overflow, record, key,
                                                    RecPtr_empty()))
                        .empty) {
                    rf_create_next_page(is->overflow);
                    last.of_ptr = rf_append_record(is->overflow, record, key,
                                                   RecPtr_empty());
                }
                /* change ptr in last */
                if (last_in_primary) {
                    rf_set_record(is->primary, last_ptr, last.record, last.key,
                                  last.deleted, last.of_ptr);
                } else { /* last not in primary */
                    rf_set_record(is->overflow, last_ptr, last.record, last.key,
                                  last.deleted, last.of_ptr);
                }
                break;
            }
        } else if (key == next->key) {
            // if (next_in_primary) {
            //     rf_set_record(is->primary, next_ptr, record, key, false,
            //                   next->of_ptr);
            // } else { /* next not in primary */
            //     rf_set_record(is->overflow, next_ptr, record, key, false,
            //                   next->of_ptr);
            // }
            PRINT_ERR("Cannot insert record with key %llu, key already exists.",
                      key);
            break;

        } else if (key > next->key) {
            last_in_primary = next_in_primary;
            last_ptr = next_ptr;
            last = *next;
            if (next_in_primary) {
                next = rf_get_next_record(is->primary);
                // next_ptr = rf_get_current_record_ptr(is->primary);
                next_ptr.offset++;
            } else { /* next not in primary */
                if (next->of_ptr.empty) {
                    next = NULL;
                    next_ptr.empty = true;
                } else { /* next ptr not empty */
                    next_ptr = next->of_ptr;
                    next = rf_get_record_global(is->overflow, next_ptr.page_no,
                                                next_ptr.offset);
                }
            }
        } else { /* key < next->key */
            if (next_in_primary) {
                if (last.of_ptr.empty) {
                    /* add to of and write ptr to last */
                    rf_goto_last_page(is->overflow);
                    if ((last.of_ptr = rf_append_record(is->overflow, record,
                                                        key, RecPtr_empty()))
                            .empty) {
                        rf_create_next_page(is->overflow);
                        last.of_ptr = rf_append_record(is->overflow, record,
                                                       key, RecPtr_empty());
                    }
                    /* change ptr in last */
                    if (last_in_primary) {
                        rf_set_record(is->primary, last_ptr, last.record,
                                      last.key, last.deleted, last.of_ptr);
                    } else { /* last not in primary */
                        rf_set_record(is->overflow, last_ptr, last.record,
                                      last.key, last.deleted, last.of_ptr);
                    }
                    break;
                } else { /* last ptr not empty */
                    /* take record from last.of_ptr as next */

                    next_ptr = last.of_ptr;
                    next = rf_get_record_global(is->overflow, next_ptr.page_no,
                                                next_ptr.offset);
                    next_in_primary = false;
                }
            } else { /* next not in primary */
                /* add to of from last.of_ptr and write ptr to last */
                struct RecPtr tmp_ptr = last.of_ptr;
                rf_goto_last_page(is->overflow);
                if ((last.of_ptr =
                         rf_append_record(is->overflow, record, key, tmp_ptr))
                        .empty) {
                    rf_create_next_page(is->overflow);
                    last.of_ptr =
                        rf_append_record(is->overflow, record, key, tmp_ptr);
                }
                /* change ptr in last */
                if (last_in_primary) {
                    rf_set_record(is->primary, last_ptr, last.record, last.key,
                                  last.deleted, last.of_ptr);
                } else { /* last not in primary */
                    rf_set_record(is->overflow, last_ptr, last.record, last.key,
                                  last.deleted, last.of_ptr);
                }
                break;
            }
        }
    }

    rf_save_current_page(is->primary);
    rf_save_current_page(is->overflow);
    is_save_metadata(is, "is.dat");
    printf("prim rec: %d, of rec: %d\n", is->primary->records_count,
           is->overflow->records_count);
    if ((float)(is->overflow->records_count) / is->primary->records_count >
        REORG_TRIG)
        is_reorganise(is, ALPHA);
    return page_no;
}

int is_update_record(struct IS *is, struct Record record, Key key, bool deleted)
{
    struct RecPtr ptr;
    bool in_primary;
    struct StoredRecord *strec = is_find_record(is, key, &ptr, &in_primary);
    if (ptr.empty) {
        return 1;
    }
    in_primary ? rf_set_record(is->primary, ptr, record, strec->key, deleted,
                               strec->of_ptr) :
                 rf_set_record(is->overflow, ptr, record, strec->key, deleted,
                               strec->of_ptr);
    rf_save_current_page(is->primary);
    rf_save_current_page(is->overflow);
    return 0;
    // return is_insert_record(is, record_minimal(), key);
}

struct StoredRecord *is_find_record(struct IS *is, Key key,
                                    struct RecPtr *out_ptr,
                                    bool *out_in_primary)
{
    int page_no = idf_find_page_to_insert(is->index, key);
    struct StoredRecord *last_prim, *next;
    bool next_in_primary = true;

    *out_ptr = RecPtr_empty();

    rf_goto_page(is->primary, page_no);

    next = rf_get_first_record_on_page(is->primary);

    while (true) {
        if (next == NULL || next->key > key) {
            if (next_in_primary) {
                if (last_prim->of_ptr.empty) {
                    next_in_primary = false;
                } else {
                    next = rf_get_record_global(is->overflow,
                                                last_prim->of_ptr.page_no,
                                                last_prim->of_ptr.offset);
                    next_in_primary = false;
                }
            } else {
                PRINT_ERR("Could not find record with key = %llu", key);
                return NULL;
            }
        } else if (next->key == key) {
            if (next->deleted) {
                PRINT_ERR("Could not find record with key = %llu", key);
                return NULL;
            } else {
                *out_in_primary = next_in_primary;
                *out_ptr = next_in_primary ?
                               rf_get_current_record_ptr(is->primary) :
                               rf_get_current_record_ptr(is->overflow);
                return next;
            }
        } else if (next->key < key) {
            if (next_in_primary) {
                last_prim = next;
                next = rf_get_next_record(is->primary);
            } else {
                if (next->of_ptr.empty) {
                    next = NULL;
                } else {
                    next =
                        rf_get_record_global(is->overflow, next->of_ptr.page_no,
                                             next->of_ptr.offset);
                }
            }
            // } else if (next->key > key) {
            //     if (next_in_primary) {
            //         if (last_prim->of_ptr.empty) {
            //             next = NULL;
            //             next_in_primary = false;
            //         } else {
            //             next = rf_get_record_global(is->overflow,
            //                                         last_prim->of_ptr.page_no,
            //                                         last_prim->of_ptr.offset);
            //             next_in_primary = false;
            //         }
            //     }
            //     else{

            //     }
        }
    }
}

int is_read_record(struct IS *is, Key key)
{
    struct RecPtr ptr;
    bool in_primary;
    struct StoredRecord *strec = is_find_record(is, key, &ptr, &in_primary);
    if (strec == NULL) {
        return 0;
    }
    struct Record rec = strec->record;
    printf("PRINT RECORD: cord_a:(%d,%d), cord_b:(%d,%d), cord_c:(%d,%d)\n",
           rec.a.x, rec.a.y, rec.b.x, rec.b.y, rec.c.x, rec.c.y);
    return 1;
}

int is_delete_record(struct IS *is, Key key)
{
    return is_update_record(is, record_minimal(), key, true);
}

int is_print_sorted_records(struct IS *is)
{
    struct StoredRecord *next;
    bool next_in_primary = true;
    rf_goto_page(is->primary, 0);
    next = rf_get_first_record_on_page(is->primary);
    struct RecPtr last_prim = { .page_no = 0, .offset = 0, .empty = false };
    int i = 0;
    printf("        PRIMARY                OVERFLOW\n");
    while (true) {
        if (next == NULL) {
            break;
        }
        if (!next->deleted) {
            if (next_in_primary)
                printf("Record nr %d: key = %llu\n", i, next->key);
            else
                printf("                   `--> Record nr %d: key = %llu\n", i,
                       next->key);
        }
        i++;
        if (!next->of_ptr.empty) {
            next = rf_get_record_global(is->overflow, next->of_ptr.page_no,
                                        next->of_ptr.offset);
            next_in_primary = false;
        } else {
            if (next_in_primary) {
                next = rf_get_next_record(is->primary);
                if (next == NULL) {
                    if (rf_goto_next_page(is->primary))
                        next = rf_get_first_record_on_page(is->primary);
                }
            } else {
                next = rf_get_record_global(is->primary, last_prim.page_no,
                                            last_prim.offset);
                next = rf_get_next_record(is->primary);
                next_in_primary = true;
                if (next == NULL) {
                    if (rf_goto_next_page(is->primary))
                        next = rf_get_first_record_on_page(is->primary);
                }
            }
            last_prim = rf_get_current_record_ptr(is->primary);
        }
    }
    return 1;
}

int is_reorganise(struct IS *is, float alpha)
{
    struct StoredRecord *next;
    bool next_in_primary = true;
    rf_goto_page(is->primary, 0);
    next = rf_get_first_record_on_page(is->primary);
    struct RecPtr last_prim = { .page_no = 0, .offset = 0, .empty = false };

    fclose(fopen("index_tmp", "wb"));
    fclose(fopen("primary_tmp", "wb"));
    fclose(fopen("overflow_tmp", "wb"));

    struct IndexFile *new_index = idf_new("index_tmp", PAGE_SIZE);
    // strncpy("index_tmp", is->index->filename,
    //         sizeof("index_tmp"));

    struct RecordFile *new_primary = rf_new("primary_tmp", PAGE_SIZE);
    // strncpy("primary_tmp", is->primary->filename,
    //         sizeof("primary_tmp"));

    struct RecordFile *new_overflow = rf_new("overflow_tmp", PAGE_SIZE);
    // strncpy("overflow_tmp", is->overflow->filename,
    //         sizeof("overflow_tmp"));

    struct Record rec = record_minimal();
    rf_create_next_page(new_primary);
    rf_append_record(new_primary, rec, 0, RecPtr_empty());
    idf_create_next_page(new_index);
    idf_append_index_global(new_index, 0, 0);
    rf_create_next_page(new_overflow);

    int records_on_page = new_primary->max_records * alpha;

    while (true) {
        if (next == NULL) {
            break;
        }
        if (!next->deleted) {
            /* save to new file */
            if (new_primary->records_num >= records_on_page) {
                rf_create_next_page(new_primary);
                rf_append_record(new_primary, next->record, next->key,
                                 RecPtr_empty());
                idf_append_index_global(new_index, next->key,
                                        new_primary->present_page_num);
            } else {
                rf_append_record(new_primary, next->record, next->key,
                                 RecPtr_empty());
            }
        }
        if (!next->of_ptr.empty) {
            next = rf_get_record_global(is->overflow, next->of_ptr.page_no,
                                        next->of_ptr.offset);
            next_in_primary = false;
        } else {
            if (next_in_primary) {
                next = rf_get_next_record(is->primary);
                if (next == NULL) {
                    if (rf_goto_next_page(is->primary))
                        next = rf_get_first_record_on_page(is->primary);
                }
            } else {
                next = rf_get_record_global(is->primary, last_prim.page_no,
                                            last_prim.offset);
                next = rf_get_next_record(is->primary);
                next_in_primary = true;
                if (next == NULL) {
                    if (rf_goto_next_page(is->primary))
                        next = rf_get_first_record_on_page(is->primary);
                }
            }
            last_prim = rf_get_current_record_ptr(is->primary);
        }
    }
    rf_free(is->primary);
    rf_free(is->overflow);
    idf_free(is->index);

    is->meta_data.index_pages = new_index->page_count;
    is->meta_data.index_records = new_index->records_count;
    is->meta_data.primary_pages = new_primary->page_count;
    is->meta_data.primary_records = new_primary->records_count;
    is->meta_data.overflow_pages = new_overflow->page_count;
    is->meta_data.overflow_records = new_overflow->records_count;

    rf_free(new_primary);
    rf_free(new_overflow);
    idf_free(new_index);

    fclose(fopen("index", "wb"));
    fclose(fopen("primary", "wb"));
    fclose(fopen("overflow", "wb"));

    rename("index_tmp", "index");
    rename("primary_tmp", "primary");
    rename("overflow_tmp", "overflow");

    is->index = idf_new("index", PAGE_SIZE); // new_index;
    is->primary = rf_new("primary", PAGE_SIZE); // new_primary;
    is->overflow = rf_new("overflow", PAGE_SIZE); // new_overflow;
    is_apply_metadata(is);
    is_save_metadata(is, "is.dat");

    is_delete_record(is, 0);

    return 1;
}

void is_debug_print(struct IS *is)
{
    if (is_verbose()) {
        verbose("INDEX:");
        idf_debug_print(is->index);
        verbose("PRIMARY:");
        rf_debug_print(is->primary);
        verbose("OVERFLOW:");
        rf_debug_print(is->overflow);
    }
}
