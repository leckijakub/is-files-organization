#include "record_file.h"
#include <errno.h>
#include <string.h>
#include <stdlib.h>

/* 
init tape record number 
@ max_buffer_b - maximum buffer size in bytes
@ page_size - system memory page size in bytes
return:
size of buffer in records
 */
int init_records_buffer(struct RecordFile *rf, int max_buffer_b, int page_size)
{
    int buff_size;

    int records_on_page;

    // rf->max_pages = max_buffer_b / (page_size - PAGE_HEADER_SIZE);
    records_on_page =
        (page_size - PAGE_HEADER_SIZE) / sizeof(struct StoredRecord);
    // buff_size = rf->max_pages * records_on_page;
    buff_size = records_on_page;
    rf->max_records = buff_size;
    PRINT_DEBUG("New rf max records: %d", buff_size);

    rf->record_buffer =
        (struct StoredRecord *)malloc(sizeof(struct StoredRecord) * buff_size);

    if (rf->record_buffer == NULL) {
        PRINT_ERR("Couldn't allocate memory for record buffer");
        return 0;
    }
    return buff_size;
}

struct RecordFile *rf_new(const char *file_name, int page_size)
{
    /* open file */
    FILE *rf_file;
    rf_file = fopen(file_name, "rb+");
    if (rf_file == NULL) {
        PRINT_DEBUG("FILE %s does not exist, creating...", file_name);
        rf_file = fopen(file_name, "wb+");
    }
    if (rf_file == NULL) {
        PRINT_ERR("Error opening a file: %s", strerror(errno));
        return NULL;
    }

    struct RecordFile *rf =
        (struct RecordFile *)malloc(sizeof(struct RecordFile));
    rf->file = rf_file;
    strcpy(rf->filename, file_name);
    rf->page_size = page_size;
    /* init buffer */
    init_records_buffer(rf, RF_MAX_BUFF_SIZE, page_size);
    rf->records_num = 0;
    rf->offset = 0;
    rf->page_count = page_count(rf->file, rf->page_size);
    rf->present_page_num = -1;
    rf->page_updated = false;
    return rf;
}

int rf_goto_page(struct RecordFile *rf, int page_number)
{
    if (page_number == rf->present_page_num) {
        PRINT_DEBUG("Already on page: %d", page_number);
        return rf->records_num;
    }
    rf_save_current_page(rf);

    // if (!page_can_read_page(rf->file, rf->page_size, page_number)) {
    if (page_number >= rf->page_count) {
        PRINT_DEBUG("Couldn't go to page: %d", page_number);
        return 0;
    }
    int records_read = 0;
    rf->records_num = 0;
    rf->offset = 0;
    records_read =
        page_read_from_file(rf->file, rf->page_size, page_number,
                            rf->record_buffer, sizeof(struct StoredRecord));

    rf->records_num = records_read;
    rf->present_page_num = page_number;
    return records_read;
}

int rf_goto_next_page(struct RecordFile *rf)
{
    if (rf_goto_page(rf, rf->present_page_num + 1)) {
        return 1;
    }
    return 0;
}

bool rf_on_last_page(struct RecordFile *rf)
{
    return rf->present_page_num == rf->page_count - 1;
    // return !page_can_read_page(rf->file, rf->page_size,
    //                            rf->present_page_num + 1);
}
bool rf_create_next_page(struct RecordFile *rf)
{
    if (!rf_on_last_page(rf)) {
        PRINT_ERR("Must be on last page to create next");
        return false;
    }
    rf_save_current_page(rf);
    rf->records_num = 0;
    rf->offset = 0;
    rf->present_page_num++;
    rf->page_count++;
    rf->page_updated = true;
    return true;
}

int rf_goto_last_page(struct RecordFile *rf)
{
    rf_save_current_page(rf);
    return rf_goto_page(rf, rf->page_count - 1);
}

struct StoredRecord *rf_get_current_record(struct RecordFile *rf)
{
    /* check if tape is empty or something wreid occurred */
    if (rf->offset >= rf->records_num) {
        PRINT_DEBUG("rf empty or some mistake, iter: %d, record_num: %d",
                    rf->offset, rf->records_num);
        return NULL;
    }
    return rf->record_buffer + rf->offset;
}

struct StoredRecord *rf_get_first_record_on_page(struct RecordFile *rf)
{
    rf->offset = 0;
    return rf_get_current_record(rf);
}

struct StoredRecord *rf_get_next_record(struct RecordFile *rf)
{
    /* check if tape is at the end of buffer */
    if (rf->offset + 1 >= rf->records_num) {
        PRINT_DEBUG("No more records on page, returning NULL");
        return NULL;
    }
    rf->offset++;
    return rf_get_current_record(rf);
}

struct StoredRecord *rf_get_record_global(struct RecordFile *rf, int page_no,
                                          int offset)
{
    if (rf_goto_page(rf, page_no) == 0) {
        PRINT_DEBUG("Couldn't get global record from page: %d", page_no);
        return NULL;
    }
    rf->offset = offset;
    return rf_get_current_record(rf);
}

struct RecPtr rf_get_current_record_ptr(struct RecordFile *rf)
{
    struct RecPtr ptr = { .page_no = rf->present_page_num,
                          .offset = rf->offset,
                          .empty = false };
    return ptr;
}

/* set record which ptr points to the given one 
*  if needed jumps to specified page and set current rec as given
*/
struct RecPtr rf_set_record(struct RecordFile *rf, struct RecPtr ptr,
                            struct Record record, Key key, bool deleted,
                            struct RecPtr of_ptr)
{
    rf_goto_page(rf, ptr.page_no);

    if (ptr.offset >= rf->records_num) {
        PRINT_DEBUG("Cannot set not existing record on page");
        ptr.empty = true;
        return ptr;
    }

    rf->offset = ptr.offset;
    struct StoredRecord sr = {
        .key = key, .deleted = deleted, .record = record, .of_ptr = of_ptr
    };
    rf->record_buffer[rf->offset] = sr;
    rf->page_updated = true;
    return ptr;
}

/* append new record to present page */
struct RecPtr rf_append_record(struct RecordFile *rf, struct Record record,
                               Key key, struct RecPtr of_ptr)
{
    struct RecPtr ptr;
    if (rf->records_num + 1 > rf->max_records) {
        PRINT_DEBUG("Couldn't append new record to page. Page full.");
        ptr.empty = true;
        return ptr;
    }

    rf->offset = rf->records_num;
    ptr.page_no = rf->present_page_num;
    ptr.offset = rf->offset;
    ptr.empty = false;
    rf->records_num++;
    rf->records_count++;

    return rf_set_record(rf, ptr, record, key, false, of_ptr);
}

void rf_save_current_page(struct RecordFile *rf)
{
    if (rf->page_updated) {
        page_write_to_file(rf->file, rf->page_size, rf->present_page_num,
                           rf->record_buffer, rf->records_num,
                           sizeof(struct StoredRecord));
    } else {
        PRINT_DEBUG("Save not needed");
    }
    rf->page_updated = false;
}

void rf_free(struct RecordFile *rf)
{
    rf_save_current_page(rf);
    fclose(rf->file);
    free(rf->record_buffer);
    free(rf);
}

void rf_debug_print(struct RecordFile *rf)
{
    struct StoredRecord *next;
    int page_no = 0;
    int ri = 0;
    char ptr_str[16];
    rf_goto_page(rf, 0);
    next = rf_get_first_record_on_page(rf);
    verbose("|Page %d ----------------------------------------------------|",
            page_no);
    while (true) {
        if (next == NULL) {
            verbose("");
            verbose("");
            break;
        } else {
            if (next->of_ptr.empty)
                sprintf(ptr_str, "none");
            else {
                sprintf(ptr_str, "page: %d, off: %d", next->of_ptr.page_no,
                        next->of_ptr.offset);
            }

            verbose(
                "|nr: %d | key: %llu | deleted: %s | ptr: (%s)              |",
                ri, next->key, next->deleted ? "true" : "false", ptr_str);
            ri++;
            next = rf_get_next_record(rf);
            if (next == NULL) {
                for (int i = 0; i < rf->max_records - rf->records_num; i++) {
                    verbose(
                        "|nr: %d | NONE                                               |",
                        ri);
                    ri++;
                }
                if (rf_goto_next_page(rf)) {
                    next = rf_get_first_record_on_page(rf);
                    if (next != NULL) {
                        page_no++;
                        ri=0;
                        verbose(
                            "|Page %d ----------------------------------------------------|",
                            page_no);
                    }
                }
            }
        }
    }
}
