#include "index_file.h"
#include "page.h"
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define IDF_MAX_BUFF_SIZE _4KiB

int idf_find_page_to_insert(struct IndexFile *idf, Key key)
{
    struct Index last, *next;

    next = idf_get_first_record_global(idf);
    last = *next;

    while (true) {
        if (next == NULL || next->key > key) {
            return last.page_no;
        } else {
            last = *next;
            next = idf_get_next_record_global(idf);
        }
    }
}

/* 
init tape record number 
@ max_buffer_b - maximum buffer size in bytes
@ page_size - system memory page size in bytes
return:
size of buffer in records
 */
int idf_init_records_buffer(struct IndexFile *idf, int max_buffer_b,
                            int page_size)
{
    int buff_size;

    int records_on_page;

    // idf->max_pages = max_buffer_b / (page_size - PAGE_HEADER_SIZE);
    records_on_page = (page_size - PAGE_HEADER_SIZE) / sizeof(struct Index);
    // buff_size = idf->max_pages * records_on_page;
    buff_size = records_on_page;
    idf->max_records = buff_size;
    PRINT_DEBUG("New idf max records: %d", buff_size);

    idf->record_buffer =
        (struct Index *)malloc(sizeof(struct Index) * buff_size);

    if (idf->record_buffer == NULL) {
        PRINT_ERR("Couldn't allocate memory for record buffer");
        return 0;
    }
    return buff_size;
}

struct IndexFile *idf_new(const char *file_name, int page_size)
{
    /* open file */
    FILE *idf_file;
    idf_file = fopen(file_name, "rb+");
    if (idf_file == NULL) {
        PRINT_DEBUG("FILE %s does not exist, creating...", file_name);
        idf_file = fopen(file_name, "wb+");
    }
    if (idf_file == NULL) {
        PRINT_ERR("Error opening a file: %s", strerror(errno));
        return NULL;
    }

    struct IndexFile *idf =
        (struct IndexFile *)malloc(sizeof(struct IndexFile));
    idf->file = idf_file;
    strcpy(idf->filename, file_name);
    idf->page_size = page_size;
    /* init buffer */
    idf_init_records_buffer(idf, IDF_MAX_BUFF_SIZE, page_size);
    idf->records_num = 0;
    idf->offset = 0;
    idf->page_count = page_count(idf->file, idf->page_size);
    idf->present_page_num = -1;
    idf->page_updated = false;
    return idf;
}

int idf_goto_page(struct IndexFile *idf, int page_number)
{
    if (page_number == idf->present_page_num) {
        PRINT_DEBUG("Already on page: %d", page_number);
        return idf->records_num;
    }
    idf_save_current_page(idf);

    // if (!page_can_read_page(idf->file, idf->page_size, page_number)) {
    if (page_number >= idf->page_count) {
        PRINT_DEBUG("Couldn't go to page: %d", page_number);
        return 0;
    }
    int records_read = 0;
    idf->records_num = 0;
    idf->offset = 0;
    records_read =
        page_read_from_file(idf->file, idf->page_size, page_number,
                            idf->record_buffer, sizeof(struct Index));

    idf->records_num = records_read;
    idf->present_page_num = page_number;
    return records_read;
}

int idf_goto_next_page(struct IndexFile *idf)
{
    if (idf_goto_page(idf, idf->present_page_num + 1)) {
        // idf->present_page_num++;
        return 1;
    }
    return 0;
}

bool idf_on_last_page(struct IndexFile *idf)
{
     return idf->present_page_num == idf->page_count - 1;
    // return !page_can_read_page(idf->file, idf->page_size,
    //                            idf->present_page_num + 1);
}
bool idf_create_next_page(struct IndexFile *idf)
{
    if (!idf_on_last_page(idf)) {
        PRINT_ERR("Must be on last page to create next");
        return false;
    }
    idf_save_current_page(idf);
    idf->records_num = 0;
    idf->offset = 0;
    idf->present_page_num++;
    idf->page_count++;
    idf->page_updated = true;
    return true;
}

int idf_goto_last_page(struct IndexFile *idf)
{
    idf_save_current_page(idf);
    return idf_goto_page(idf, idf->page_count - 1);
}

struct Index *idf_get_current_record(struct IndexFile *idf)
{
    /* check if tape is empty or something wreid occurred */
    if (idf->offset >= idf->records_num) {
        PRINT_DEBUG("idf empty or some mistake, iter: %d, record_num: %d",
                    idf->offset, idf->records_num);
        return NULL;
    }
    return idf->record_buffer + idf->offset;
}

struct Index *idf_get_first_record_global(struct IndexFile *idf)
{
    idf_goto_page(idf, 0);
    idf->offset = 0;
    return idf_get_current_record(idf);
}

struct Index *idf_get_first_record_on_page(struct IndexFile *idf)
{
    idf->offset = 0;
    return idf_get_current_record(idf);
}

struct Index *idf_get_next_record(struct IndexFile *idf)
{
    /* check if tape is at the end of buffer */
    if (idf->offset + 1 >= idf->records_num) {
        PRINT_DEBUG("No more records on page, returning NULL");
        return NULL;
    }
    idf->offset++;
    return idf_get_current_record(idf);
}

struct Index *idf_get_next_record_global(struct IndexFile *idf)
{
    /* check if tape is at the end of buffer */
    if (idf->offset + 1 >= idf->records_num) {
        if (!idf_goto_next_page(idf)) {
            PRINT_DEBUG("No more indexes, returning NULL");
            return NULL;
        }
    } else {
        idf->offset++;
    }
    return idf_get_current_record(idf);
}

struct Index *idf_get_record_global(struct IndexFile *idf, int page_no,
                                    int offset)
{
    if (idf_goto_page(idf, page_no) == 0) {
        PRINT_DEBUG("Couldn't get global record from page: %d", page_no);
        return NULL;
    }
    idf->offset = offset;
    return idf_get_current_record(idf);
}

struct RecPtr idf_get_current_record_ptr(struct IndexFile *idf)
{
    struct RecPtr ptr = { .page_no = idf->present_page_num,
                          .offset = idf->offset,
                          .empty = false };
    return ptr;
}

/* set record which ptr points to the given one 
*  if needed jumps to specified page and set current rec as given
*/
struct RecPtr idf_set_record(struct IndexFile *idf, struct RecPtr ptr, Key key,
                             int page_no)
{
    idf_goto_page(idf, ptr.page_no);

    if (ptr.offset >= idf->records_num) {
        PRINT_DEBUG("Cannot set not existing record on page");
        ptr.empty = true;
        return ptr;
    }

    idf->offset = ptr.offset;
    struct Index sr = { .key = key, .page_no = page_no };
    idf->record_buffer[idf->offset] = sr;
    idf->page_updated = true;
    return ptr;
}

/* append new record to present page */
struct RecPtr idf_append_index_global(struct IndexFile *idf, Key key,
                                      int page_no)
{
    idf_goto_last_page(idf);
    struct RecPtr ptr;
    if (idf->records_num + 1 > idf->max_records) {
        idf_create_next_page(idf);
    }

    idf->offset = idf->records_num;
    // if (idf->records_num > 0) {
    //     idf->offset++;
    // }
    ptr.page_no = idf->present_page_num;
    ptr.offset = idf->offset;
    ptr.empty = false;
    idf->records_num++;
    idf->records_count++;

    return idf_set_record(idf, ptr, key, page_no);
}

void idf_save_current_page(struct IndexFile *idf)
{
    if (idf->page_updated) {
        page_write_to_file(idf->file, idf->page_size, idf->present_page_num,
                           idf->record_buffer, idf->records_num,
                           sizeof(struct Index));
    } else {
        PRINT_DEBUG("Save not needed");
    }
    idf->page_updated = false;
}

void idf_free(struct IndexFile *idf)
{
    idf_save_current_page(idf);
    fclose(idf->file);
    free(idf->record_buffer);
    free(idf);
}

void idf_debug_print(struct IndexFile *idf)
{
    struct Index *next;
    int page_no = 0;
    int ri = 0;
    idf_goto_page(idf, 0);
    next = idf_get_first_record_on_page(idf);
    verbose("|Page %d -------------------------------|", page_no);
    while (true) {
        if (next == NULL) {
            verbose("");
            verbose("");
            break;
        } else {
            verbose("|nr: %d | key: %llu | page: %d              |", ri,
                    next->key, next->page_no);
            ri++;
            next = idf_get_next_record(idf);
            if (next == NULL) {
                for (int i = 0; i < idf->max_records - idf->records_num; i++) {
                    verbose("|nr: %d | NONE                          |", ri);
                    ri++;
                }
                if (idf_goto_next_page(idf)) {
                    next = idf_get_first_record_on_page(idf);
                    if (next != NULL) {
                        page_no++;
                        ri=0;
                        verbose("|Page %d -------------------------------|",
                                page_no);
                    }
                }
            }
        }
    }
}
