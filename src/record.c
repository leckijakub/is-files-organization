#include <stdlib.h>
#include <math.h>
#include "record.h"
#include "utils.h"

struct Record *record_new(struct Coord a, struct Coord b, struct Coord c)
{
    struct Record *rec = malloc(sizeof(struct Record));
    rec->a = a;
    rec->b = b;
    rec->c = c;
    return rec;
}

struct Record record_minimal()
{
    struct Record rec = { .a = { 0, 0 }, .b = { 0, 0 }, .c = { 0, 0 } };
    return rec;
}

double distance(struct Coord a, struct Coord b)
{
    return sqrt(pow(abs(a.x - b.x), 2) + pow(abs(a.y - b.y), 2));
}

double coord_eq(struct Coord a, struct Coord b)
{
    return a.x == b.x && a.y == b.y;
}

double record_key(struct Record *rec)
{
    double x = distance(rec->a, rec->b);
    double y = distance(rec->b, rec->c);
    double z = distance(rec->c, rec->a);
    double p = (x + y + z) / 2.0;
    return sqrt(p * (p - x) * (p - y) * (p - z));
}

int record_cmp(struct Record *rec1, struct Record *rec2)
{
    double key1, key2;
    key1 = record_key(rec1);
    key2 = record_key(rec2);
    if (key1 > key2) {
        return 1;
    } else if (key1 == key2) {
        return 0;
    } else
        return -1;
}
