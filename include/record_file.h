#ifndef RECORD_FILE_H
#define RECORD_FILE_H

#include <stdio.h>
#include "record.h"
#include "utils.h"
#include "page.h"
#define RF_MAX_BUFF_SIZE _4KiB

struct RecordFile {
    FILE *file;
    char filename[256];
    int page_size;
    int max_pages;
    int max_records;
    struct StoredRecord *record_buffer;
    int records_num;
    int offset;
    int present_page_num;
    int page_count;
    int records_count;
    int page_updated;
};

struct RecordFile *rf_new(const char *file_name, int page_size);
int rf_goto_page(struct RecordFile *pf, int page_number);
int rf_goto_next_page(struct RecordFile *pf);
int rf_goto_last_page(struct RecordFile *pf);

bool rf_on_last_page(struct RecordFile *pf);
bool rf_create_next_page(struct RecordFile *pf);

struct StoredRecord *rf_get_next_record(struct RecordFile *pf);
struct StoredRecord *rf_get_first_record_on_page(struct RecordFile *rf);
struct StoredRecord *rf_get_record_global(struct RecordFile *rf, int page_no,
                                          int offset);

struct RecPtr rf_get_current_record_ptr(struct RecordFile *rf);

struct RecPtr rf_append_record(struct RecordFile *rf, struct Record record,
                               Key key, struct RecPtr of_ptr);

struct RecPtr rf_set_record(struct RecordFile *rf, struct RecPtr ptr,
                            struct Record record, Key key, bool deleted,
                            struct RecPtr of_ptr);

void rf_save_current_page(struct RecordFile *rf);

void rf_debug_print(struct RecordFile *rf);

void rf_free(struct RecordFile *rf);

#endif
