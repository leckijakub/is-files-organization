#ifndef PAGE_H
#define PAGE_H

#include <stdio.h>
#include "record.h"
#include "utils.h"

#define PAGE_HEADER_SIZE 4

struct Page {
    struct Record *records_buff;
    int records_num;
    int page_size;
};

struct PageHeader {
    int records_on_page;
};

int page_read_from_file(FILE *file, int page_size, int page_no, void *dest,
                        int record_size);
int page_write_to_file(FILE *file, int page_size, int page_no, void *source,
                       int count, int record_size);

bool page_can_read_page(FILE *file, int page_size, int page_no);

int page_count(FILE *file, int page_size);

void page_free(struct Page *page);

int page_read_count();
void page_set_read(int read);
int page_write_count();
void page_set_write(int write);
#endif
