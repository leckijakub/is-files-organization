#ifndef UTILS_H
#define UTILS_H

#include <stdio.h>
#include <stdbool.h>

#define COLOR_RED "\033[0;31m"
#define COLOR_NORMAL "\033[0m"
#define COLOR_GREEN "\033[0;32m"
#define COLOR_BOLD_GREEN "\033[1;32m"
#define COLOR_BLUE "\033[0;34m"
#define COLOR_YELLOW "\033[01;33m"

#define _4KiB 4096
#define _8KiB 8192
#define _1GiB 1073741824


#define PRINT_ERR(...)                       \
    do{                                      \
        fprintf(stderr, "[");                \
        fprintf(stderr, COLOR_RED "ERROR");  \
        fprintf(stderr, COLOR_NORMAL "]: "); \
        fprintf(stderr, __VA_ARGS__);        \
        fprintf(stderr, "\n");               \
    }while(0)

#ifdef DEBUG

#define PRINT_DEBUG(...)                     \
    do{                                      \
        fprintf(stdout, "[");                \
        fprintf(stdout, COLOR_BLUE "DEBUG");  \
        fprintf(stdout, COLOR_NORMAL "]: "); \
        fprintf(stdout, __VA_ARGS__);        \
        fprintf(stdout, "\n");               \
    }while(0)

#else
#define PRINT_DEBUG(...)
#endif

int verbose(const char* fmt, ...);
void set_verbose(bool verbose);
bool is_verbose();
#endif
