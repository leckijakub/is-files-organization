#ifndef RECORD_H
#define RECORD_H

#include "key.h"
#include "is_utils.h"
struct Coord {
    int x,y;
};

struct Record {
    struct Coord a;
    struct Coord b;
    struct Coord c;
};

struct StoredRecord{
    Key key;
    bool deleted;
    struct Record record;
    struct RecPtr of_ptr;
};

struct Record *record_new(struct Coord a, struct Coord b, struct Coord c);

double distance(struct Coord a, struct Coord b );
double coord_eq(struct Coord a, struct Coord b );
int record_cmp(struct Record* rec1,struct Record* rec2);
double record_key(struct Record *rec);
struct Record record_minimal();

#endif
