#ifndef IS_UTILS_H
#define IS_UTILS_H

#include <stdbool.h>
struct RecPtr{
    unsigned int page_no;
    unsigned int offset;
    bool empty;
};
struct RecPtr RecPtr_empty();
#endif
