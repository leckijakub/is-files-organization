#ifndef INDEX_FILE_H
#define INDEX_FILE_H

#include <stdio.h>
#include <stdbool.h>
#include "key.h"

struct Index {
    Key key;
    int page_no;
};

struct IndexFile {
    FILE *file;
    char filename[256];
    int page_size;
    int max_pages;
    int max_records;
    struct Index *record_buffer;
    int records_num;
    int offset;
    int present_page_num;
    int page_count;
    int records_count;
    int page_updated;
};

struct IndexFile *idf_new(const char *file_name, int page_size);
void idf_free(struct IndexFile *idf);

int idf_find_page_to_insert(struct IndexFile *idf, Key key);

struct RecPtr idf_append_index_global(struct IndexFile *idf, Key key, int page_no);

struct Index *idf_get_first_record_global(struct IndexFile *idf);
struct Index *idf_get_next_record_global(struct IndexFile *idf);

void idf_save_current_page(struct IndexFile *idf);

bool idf_create_next_page(struct IndexFile *idf);

void idf_debug_print(struct IndexFile *idf);

#endif
