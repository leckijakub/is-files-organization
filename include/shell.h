#ifndef SHELL_H

#define SHELL_H

int shell_interact();
int shell_script(char *filename);
int shell_reorganise(char **args);
int shell_insert(char **args);
int shell_update(char **args);
int shell_delete(char **args);
int shell_read(char **args);
int shell_show(char **args);
int shell_exit(char **args);
int shell_history();
int shell_help();

struct IS *is;
#endif
