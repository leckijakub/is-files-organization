#ifndef IS_H

#define IS_H
#include "record_file.h"
#include "index_file.h"
struct IS_metadata{
    int index_pages;
    int index_records;
    int primary_pages;
    int primary_records;
    int overflow_pages;
    int overflow_records;
};

struct IS {
    struct IndexFile *index;
    struct RecordFile *primary;
    struct RecordFile *overflow;
    struct IS_metadata meta_data;
};

int PAGE_SIZE;
float REORG_TRIG;
float ALPHA;

struct IS *is_new(const char *id_filename, const char *prim_filename,
                  const char *of_filename);
void is_save_metadata(struct IS *is, const char *filename);
int is_insert_record(struct IS *is, struct Record record, Key key);

int is_update_record(struct IS *is, struct Record record, Key key,
                     bool deleted);

int is_delete_record(struct IS *is, Key key);

int is_read_record(struct IS *is, Key key);

struct StoredRecord *is_find_record(struct IS *is, Key key,
                                    struct RecPtr *out_ptr,
                                    bool *out_in_primary);

int is_print_sorted_records(struct IS *is);

int is_print_index(struct IS *is);

int is_reorganise(struct IS *is, float alpha);

void is_debug_print(struct IS *is);

void is_free(struct IS *is);

#endif
