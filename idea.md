# Project idea

# Structure

One `IS` file consist of three partial files:
* `Index file`
* `Primary area`
* `Overflow area` 

## Index file
The file consists of `IndexRecords`. Every `IndexRecord` has to fields:
* Key - the key value of the first record on the page
* PagePtr - pointer to that page

Index Records are always sorted by Key value. It happens while
creating/appending to index file so no need to implement manual sorting.

## Primary area

The file consists of pages with StoredRecords. On every page `StoredRecords` are
sorted during insertion.

### StoredRecords

Consists of:

* Key - the key value of the `Record`
* Record - real record with data
* of_ptr - pointer to next record in `Overflow Area`


## Operations

### Insert record

* find last page with records smaller than new
  * go through all indexes
  * remember last index
  * go to next
  * if next > new; then return last;fi
  * last=next - if whole index will be copied (not referenced) there will be no
    need to go back to previous page

* insert record
  * last=null - may be reference - we operate only on one page
  * next = get first
  * go through all records on page
  * 
  * if next == null; then 
    * if there is space left on page; then append record to page after last record
    * else create new page, insert record and add index in index file
    * return
  * if next > new; then
    * TODO
  * else
    * 


## Bugs

```shell
[VERBOSE]:         PRIMARY                OVERFLOW
[VERBOSE]: Record nr 0: key = 0
[VERBOSE]: Record nr 1: key = 1
[VERBOSE]:                    `--> Record nr 2: key = 2
[VERBOSE]: Record nr 3: key = 10
[VERBOSE]:                    `--> Record nr 4: key = 11
[VERBOSE]: Record nr 5: key = 15
[VERBOSE]:                    `--> Record nr 6: key = 16
[VERBOSE]: Record nr 7: key = 120
[VERBOSE]:                    `--> Record nr 8: key = 121
[VERBOSE]: Record nr 9: key = 16121
jal@jal-pc:~/study/sem_5/sbd/is-files-organization (develop)$ ./build/src/is -v -i 117
[VERBOSE]:         PRIMARY                OVERFLOW
[VERBOSE]: Record nr 0: key = 0
[VERBOSE]: Record nr 1: key = 1
[VERBOSE]:                    `--> Record nr 2: key = 2
[VERBOSE]: Record nr 3: key = 10
[VERBOSE]:                    `--> Record nr 4: key = 11
[VERBOSE]: Record nr 5: key = 15
[VERBOSE]:                    `--> Record nr 6: key = 16

```
## TODO

* deleting not existing key must be imposible
